/**
 * Created by BURGOS on 12/09/2018.
 */

const authorSchema = require('../../models/author');

class AuthorService {
  constructor () {}

  async getAllAuthors () {
    return await authorSchema.find({})
  }

  async getAuthorById (idAuthor) {
    return await authorSchema.findById(idAuthor);
  }

  async postAuthor (author) {
    return await authorSchema.create(author)
  }

  async putAuthor (authorId, author) {
    return await authorSchema.findByIdAndUpdate(authorId, author)
  }

  async deleteAuthor (authorId) {
    return await authorSchema.findByIdAndRemove(authorId);
  }
}

module.exports = AuthorService;