/**
 * Created by BURGOS on 14/01/2018.
 */

const mongoose = require('mongoose'),
    schema = mongoose.Schema,
    courseSchema = schema({
        name: String,
        description: String,
        idAuthor: { type: schema.ObjectId, ref: 'authors' },
        idPlatform: { type: schema.ObjectId, ref: 'platforms' },
        idPlace: { type: schema.ObjectId, ref: 'places' },
        categories: [{type: schema.ObjectId, ref: 'categories'}]
    });

module.exports = mongoose.model('courses', courseSchema);