/**
 * Created by BURGOS on 10/01/2018.
 */
class PlatformRoute {
    constructor(route) {
        this.route = route.Router();
        this.Platform = require('../controllers/platform.controller')
    }

    getRoute() {
        this.defineRoutes();
        return this.route;
    }

    defineRoutes() {
        const platform = new this.Platform();
        this.route.get('/platform', platform.getAllPlatforms);
        this.route.get('/platform/:id', platform.getPlatformById);
        this.route.post('/platform', platform.postNewPlatform);
        this.route.put('/platform/:id', platform.putPlatform);
        this.route.delete('/platform/:id', platform.deletePlatform);
    }
}

module.exports = PlatformRoute;