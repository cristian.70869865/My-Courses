/**
 * Created by BURGOS on 13/09/2018.
 */

const AuthorHttpResponse = require('./author.httpResponse');

class AuthorValidate {
  constructor () {}

  validationToGetAllAuthors (authors) {
    if (!authors.length)
      return AuthorHttpResponse.notFound();

    return {
      code: 200,
      data: authors
    }
  }

  validationToGetAuthorById (author) {
    if (author === null)
      return AuthorHttpResponse.notFound();

    return {
      code: 200,
      data: author
    }
  }

  validationToPostAuthor () {
    return AuthorHttpResponse.createdSuccessfully();
  }

  validationToPutAuthor (author) {
    if (author === null)
      return AuthorHttpResponse.notFound();

    return AuthorHttpResponse.updatedSuccessfully();
  }

  validationToDeleteAuthor (author) {
    if (author === null)
      return AuthorHttpResponse.notFound();

    return AuthorHttpResponse.deletedSuccessfully();
  }
}

module.exports = AuthorValidate;