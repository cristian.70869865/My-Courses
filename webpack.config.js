/**
 * Created by BURGOS on 25/10/2018.
 */

const webpack = require('webpack');
const path = require('path');
const fs = require('fs');

let nodeModules = {};
fs.readdirSync('node_modules')
  .filter( (x) => {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach( (mod) => {
    nodeModules[mod] = 'commonjs ' + mod;
  });

module.exports = {
  mode: 'production',
  entry: './server',
  target: 'node',
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'backend.min.js'
  },
  externals: nodeModules
};