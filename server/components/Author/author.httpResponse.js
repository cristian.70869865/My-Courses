/**
 * Created by BURGOS on 23/10/2018.
 */

class AuthorHttpResponse {
  constructor () {}

  static updatedSuccessfully () {
  return {
    code: 200,
    data: {
      message: "Se actualizo con exito"
    }
  }
}

  static deletedSuccessfully () {
  return {
    code: 200,
    data: {
      message: "Se elimino con exito"
    }
  }
}

  static createdSuccessfully () {
  return {
    code: 201,
    data: {
      message: "Se creo con exito"
    }
  }
}

  static badRequest () {
  return {
    code: 400,
    data: {
      message: 'Rellene todos los campos'
    }
  }
}

  static notFound () {
  return {
    code: 404,
    data: {
      message: "No se encontro ningun autor"
    }
  }
}

  static internalServerError () {
  return {
    code: 500,
    data: {
      message: "Problema interno del servidor"
    }
  }
}
}

module.exports = AuthorHttpResponse;