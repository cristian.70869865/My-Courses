/**
 * Created by BURGOS on 24/10/2018.
 */

const AuthorHttpResponse = require('./author.httpResponse');

module.exports = function (error) {
  if (error.name === 'ValidationError')
    return validationError(error.message);
  if (error.name === 'CastError')
    return castError(error.message);

  return AuthorHttpResponse.internalServerError();
};

function validationError (errorMessage) {
  return AuthorHttpResponse.badRequest();
}

function castError (errorMessage) {
  return AuthorHttpResponse.notFound();
}