/**
 * Created by BURGOS on 09/01/2018.
 */
class PlaceRoute {
    constructor(route) {
        this.route = route.Router();
        this.Place = require('../controllers/place.controller')
    }

    getRoute() {
        this.defineRoutes();
        return this.route;
    }

    defineRoutes() {
        const place = new this.Place();
        this.route.get('/place', place.getAllPlaces);
        this.route.get('/place/:id', place.getPlaceById);
        this.route.post('/place', place.postNewPlace);
        this.route.put('/place/:id', place.putPlace);
        this.route.delete('/place/:id', place.deletePlace);
    }
}

module.exports = PlaceRoute;