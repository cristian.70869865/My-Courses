/**
 * Created by BURGOS on 14/01/2018.
 */

const courseModel = require('../models/course.model');

class CourseController {
    constructor() {}

    getAllCourses(req, res) {
        courseModel.find({})
            .populate({path: 'idAuthor'})
            .populate({path: 'idPlatform'})
            .populate({path: 'idPlace'})
            .exec( (error, courses) => {
            if(error)
                return res.status(500).send({code: 500, data: {message:'Problema interno del servidor'}});
            if(courses.length === 0)
                return res.status(404).send({code: 404, data: {message:'Recurso no encontrado'}});
            res.status(200).send({code: 200, data: courses});
        })
    }

    getCourseById(req, res) {
        let idCourse = req.params.id;
        courseModel.findById(idCourse)
            .populate({path: 'idAuthor'})
            .populate({path: 'idPlatform'})
            .populate({path: 'idPlace'})
            .exec( (error, course) => {
            if(error)
                return res.status(404).send({code: 404, data: {message: 'Recurso no encontrado.'}});
            res.status(200).send({code: 200, data: course})
        })
    }

    postSearchCourse(req, res) {
        try {
            let search              = req.body.search.toLowerCase(),
                newResultSearch     = [];
            search              = search.replace(/ /g, '.*');
            if(Object.entries(search).length === 0)
                return res.status(400).send({code: 400, data: {message: 'Rellene todos los campos'}});
            courseModel.find({})
                .populate('idPlace')
                .populate('idPlatform')
                .populate('idAuthor')
                .populate('categories')
                .exec((error, resultSearch) => {
                    resultSearch.forEach( (course) => {
                        let courseName = course.name.toLowerCase();
                        if(courseName.match(search))
                            newResultSearch.push(course);
                    });
                    if(newResultSearch.length === 0)
                        return res.status(404).send({code: 404, data: {message: 'No se encontro ningun curso'}});
                    res.status(200).send({code: 200, data: newResultSearch})
                })
        } catch(error) {
            return res.status(500).send({code: 500, data: {message: 'Problema interno del servidor'}})
        }

    }

    postNewCourse(req, res) {
        let course         = new courseModel();
        course.name        = req.body.name;
        course.description = req.body.description;
        course.idAuthor    = req.body.idAuthor;
        course.idPlatform  = req.body.idPlatform;
        course.idPlace     = req.body.idPlace;
        course.categories  = req.body.categories;
        if(course.name === undefined || course.description === undefined || course.idAuthor === undefined ||
            course.idPlatform === undefined || course.idPlace === undefined)
            return res.status(400).send({code: 400, data: {message: 'Rellene todos los campos.'}});
        course.save( (error, courseSave) => {
            if(error)
                return res.status(500).send({code: 500, data: {message:'Problema interno del servidor'}});
            res.status(201).send({code: 201, data: courseSave});
        })
    }

    putCourse(req, res) {
        let idCourse   = req.params.id,
            bodyCourse = req.body;
        if(Object.entries(bodyCourse).length === 0)
            return res.status(400).send({code: 400, data: {message: 'Rellene todos los campos'}});
        courseModel.findByIdAndUpdate(idCourse, bodyCourse, (error, updatedCourse) => {
            if(error)
                return res.status(404).send({code: 404, data: {message:'Recurso no encontrado'}});
            res.status(200).send({code: 200, data: updatedCourse});
        })
    }

    deleteCourse(req, res) {
        let idCourse = req.params.id;
        courseModel.findById(idCourse, (error, deletingCourse) => {
            if(error !== null)
                return res.status(404).send({code: 404, data: {message: 'Recurso no encontrado.'}});
            deletingCourse.remove(error => {
                if(error)
                    return res.status(500).send({code: 500, data: {message:'Error al intentar borrar el registro.'}})
            });
            res.status(200).send({code: 200, data: {message: 'Se elimino correctamente'}});
        })
    }
}

module.exports = CourseController;