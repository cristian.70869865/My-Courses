/**
 * Created by BURGOS on 11/01/2018.
 */

const mongoose = require('mongoose'),
    schema = mongoose.Schema,
    categorySchema = schema({
        name: String
    });

module.exports = mongoose.model('categories', categorySchema);