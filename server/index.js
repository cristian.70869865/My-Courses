/**
 * Created by BURGOS on 09/01/2018.
 */

const express    = require('express');
const mongoose   = require('mongoose');
const Aplication = require('./app');
const app    = new Aplication(express).getAplication();
const config = require('./config');

mongoose.connect(config.db, {useMongoClient: true}, (error, response) => {
  if(error)
    console.log('Error al conectar con la base de datos: ' + error);
  if(!error) {
    app.listen(config.port, () => {
      console.log('API Rest corriendo en puerto 3001');
    });
  }
});
