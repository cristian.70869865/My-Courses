/**
 * Created by BURGOS on 09/01/2018.
 */

class Aplication {
  constructor(express) {
    this.app   = express();
    this.route = express;
    this.bodyParser = require('body-parser');
  }

  getAplication() {
    const placeRoute    = require('./routes/place.route');
    const platformRoute = require('./routes/platorm.route');
    const categoryRoute = require('./routes/category.route');
    const authorRoute   = require('./components/Author/author.route');
    const courseRoute   = require('./routes/course.route');
    this.app.use(this.bodyParser.urlencoded({extended: false}));
    this.app.use(this.bodyParser.json());
    this.app.use(this.route.static(__dirname + '/public'));
    this.defineCors();
    this.defineRoutesToVersionOne(new placeRoute(this.route), new platformRoute(this.route), new categoryRoute(this.route),
                                  new authorRoute(this.route), new courseRoute(this.route));
    return this.app;
  }

  defineRoutesToVersionOne(placeRoute, platformRoute, categoryRoute, authorRoute, courseRoute) {
    this.app.use('/api/v1', placeRoute.getRoute());
    this.app.use('/api/v1', platformRoute.getRoute());
    this.app.use('/api/v1', categoryRoute.getRoute());
    this.app.use('/api/v1', authorRoute.getRoute());
    this.app.use('/api/v1', courseRoute.getRoute());
  }

  defineCors() {
    this.app.all('/*', function(req, res, next){
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, " +
          "Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
      res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
      next();
    });
  }
}

module.exports = Aplication;