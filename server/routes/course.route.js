/**
 * Created by BURGOS on 14/01/2018.
 */

class CourseRoute {
    constructor(route) {
        this.route = route.Router();
        this.courseController = require('../controllers/course.controller')
    }

    getRoute() {
        this.defineRoutes();
        return this.route;
    }

    defineRoutes() {
        const course = new this.courseController();
        this.route.get('/course', course.getAllCourses);
        this.route.get('/course/:id', course.getCourseById);
        this.route.post('/searchCourse', course.postSearchCourse);
        this.route.post('/course', course.postNewCourse);
        this.route.put('/course/:id', course.putCourse);
        this.route.delete('/course/:id', course.deleteCourse);
    }
}

module.exports = CourseRoute;