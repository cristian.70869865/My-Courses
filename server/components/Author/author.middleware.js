/**
 * Created by BURGOS on 30/10/2018.
 */

const AuthorRequest = require('./author.request');

class AuthorMiddleware {
  constructor () {
    this.authorRequest = new AuthorRequest();
  }

  validateFieldOfAuthor (req, res, next) {
    let httpResponse = this.authorRequest.requestToPutAuthor(req.body);

    if (httpResponse.code !== undefined)
      return res.status(httpResponse.code)
        .send(httpResponse);

    req.requestToPutAuthor = httpResponse;
    next();
  }
}

module.exports = AuthorMiddleware;