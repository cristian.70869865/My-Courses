/**
 * Created by BURGOS on 10/01/2018.
 */
const platformModel = require('../models/platform.model');

class PlatformController {
    constructor() {}

    getAllPlatforms(req, res) {
        platformModel.find({}, (error, platforms) => {
            if(error)
                return res.status(500).send({code: 500, data: {message:'Problema interno del servidor'}});
            if(platforms.length === 0)
                return res.status(404).send({code: 404, data: {message:'Recurso no encontrado'}});
            res.status(200).send({code: 200, data: platforms});
        })
    }

    getPlatformById(req, res) {
        let idPlatform = req.params.id;
        platformModel.findById(idPlatform, (error, platform) => {
            if(error)
                return res.status(404).send({code: 404, data: {message: 'Recurso no encontrado.'}});
            res.status(200).send({code: 200, data: platform})
        })
    }

    postNewPlatform(req, res) {
        let platform  = new platformModel();
        platform.name = req.body.name;
        if(platform.name === undefined)
            return res.status(400).send({code: 400, data: {message: 'Rellene todos los campos.'}});
        platform.save( (error, platformSave) => {
            if(error)
                return res.status(500).send({code: 500, data: {message:'Problema interno del servidor'}});
            return res.status(201).send({code: 201, data: platformSave});
        })
    }

    putPlatform(req, res) {
        let idPlatform   = req.params.id,
            bodyPlatform = req.body;
        if(Object.entries(bodyPlatform).length === 0)
            return res.status(400).send({code: 400, data: {message: 'Rellene todos los campos'}});
        platformModel.findByIdAndUpdate(idPlatform, bodyPlatform, (error, updatedPlatform) => {
            if(error)
                return res.status(404).send({code: 404, data: {message:'Recurso no encontrado'}});
            res.status(200).send({code: 200, data: updatedPlatform});
        })
    }

    deletePlatform(req, res) {
        let idPlatform = req.params.id;
        platformModel.findById(idPlatform, (error, deletingPlatform) => {
            if(error !== null)
                return res.status(404).send({code: 404, data: {message: 'Recurso no encontrado.'}});
            deletingPlatform.remove(error => {
                if(error)
                    return res.status(204).send({code: 204, data: {message:'Error al intentar borrar el registro.'}})
            });
            res.status(200).send({code: 200, data: {message: 'Se elimino correctamente'}});
        })
    }
}

module.exports = PlatformController;