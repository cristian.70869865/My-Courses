/**
 * Created by BURGOS on 09/01/2018.
 */
const placeModel = require('../models/place.model');
class PlaceController {
    constructor() {}

    getAllPlaces(req, res) {
        placeModel.find({}, (error, places) => {
            if(error)
                 return res.status(500).send({code: 500, data: {message: 'Problema interno del servidor'}});
            if(places.length === 0)
                 return res.status(404).send({code: 404, data: 'Recurso no encontrado.'});
             res.status(200).send({code: 200, data: places});
        });
    }

    getPlaceById(req, res) {
        let id = req.params.id;
        placeModel.findById(id, (error, place) => {
            if(error)
                return res.status(404).send({code: 404, data: {message:'Recurso no encontrado.'}});
            res.status(200).send({code: 200, data: place});
        })
    }

    postNewPlace(req, res) {
        let place = new placeModel();
        place.name = req.body.name;
        if(place.name === undefined)
            return res.status(400).send({code: 400, data: {message: 'Rellene todos los campos.'}});
        place.save( (error, placeSave) => {
            if(error)
                return res.status(500).send({code: 500, data: {message:'Problema interno del servidor'}});
            res.status(201).send({code: 201, data: placeSave});
        })
    }

    putPlace(req, res) {
        let idPlace   = req.params.id,
            bodyPlace = req.body;
        if(Object.entries(bodyPlace).length === 0)
            return res.status(400).send({code: 400, data: {message: 'Rellene todos los campos'}});
        placeModel.findByIdAndUpdate(idPlace, bodyPlace, (error, updatedPlace) => {
            if(error)
                return res.status(404).send({code: 404, data: {message:'Recurso no encontrado'}});
            res.status(200).send({code: 200, data: updatedPlace});
        })
    }

    deletePlace(req, res) {
        let idPlace = req.params.id;
        placeModel.findById(idPlace, (error, deletingPlace) => {
            if(error !== null)
                return res.status(404).send({code: 404, data: {message: 'Recurso no encontrado.'}});
            deletingPlace.remove(error => {
                if(error)
                    return res.status(204).send({code: 204, data: {message:'Error al intentar borrar el registro.'}})
            });
            res.status(200).send({code: 200, data: {message: 'Se elimino correctamente'}});
        })
    }
}

module.exports = PlaceController;