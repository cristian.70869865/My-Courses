/**
 * Created by BURGOS on 14/01/2018.
 */

const mongoose = require('mongoose');
const schema   = mongoose.Schema;
const authorSchema = schema({
  name: {
    type: String,
    trim: true,
    required: true
  }
});

module.exports = mongoose.model('authors', authorSchema);