/**
 * Created by BURGOS on 14/01/2018.
 */

const AuthorRequest  = require('./author.request');
const AuthorService  = require('./author.service');
const AuthorValidate = require('./author.validate');
const AuthorError = require('./author.error');

class AuthorController {
  constructor () {
    this.authorRequest  = new AuthorRequest();
    this.authorService  = new AuthorService();
    this.authorValidate = new AuthorValidate();
  }

  async getAllAuthors (req, res) {
    let authors = [];
    let httpResponse = {};

    try {
      authors = await this.authorService.getAllAuthors();
      httpResponse = this.authorValidate.validationToGetAllAuthors(authors);

      return res.status(httpResponse.code)
        .send(httpResponse);
    } catch (error) {
      httpResponse = AuthorError(error);

      return res.status(httpResponse.code)
        .send(httpResponse);
    }
  }

  async getAuthorById (req, res) {
    let author = {};
    let httpResponse = {};

    try {
      author = await this.authorService.getAuthorById(req.params.id);
      httpResponse = this.authorValidate.validationToGetAuthorById(author);

      return res.status(httpResponse.code)
        .send(httpResponse);
    } catch (error) {
      httpResponse = AuthorError(error);

      return res.status(httpResponse.code)
        .send(httpResponse);
    }
  }

  async postNewAuthor (req, res) {
    let authorRequest = {};
    let httpResponse  = {};

    try {
      authorRequest = this.authorRequest.requestToPostAuthor(req.body);
      await this.authorService.postAuthor(authorRequest);
      httpResponse = this.authorValidate.validationToPostAuthor();

      return res.status(httpResponse.code)
        .send(httpResponse);
    } catch (error) {
      httpResponse = AuthorError(error);

      return res.status(httpResponse.code)
        .send(httpResponse);
    }
  }

  async putAuthor (req, res) {
    let authorRequest = {};
    let authorUpdated = {};
    let httpResponse  = {};

    try {
      authorRequest = req.requestToPutAuthor;
      authorUpdated = await this.authorService.putAuthor(req.params.id, authorRequest);
      httpResponse  = this.authorValidate.validationToPutAuthor(authorUpdated);

      return res.status(httpResponse.code)
        .send(httpResponse);
    } catch (error) {
      httpResponse = AuthorError(error);

      return res.status(httpResponse.code)
        .send(httpResponse);
    }
  }

  async deleteAuthor (req, res) {
    let authorDeleted = {};
    let httpResponse  = {};

    try {
      authorDeleted = await this.authorService.deleteAuthor(req.params.id);
      httpResponse  = this.authorValidate.validationToDeleteAuthor(authorDeleted);

      return res.status(httpResponse.code)
        .send(httpResponse);
    } catch (error) {
      httpResponse = AuthorError(error);

      res.status(httpResponse.code)
        .send(httpResponse)
    }
  }
}

module.exports = AuthorController;