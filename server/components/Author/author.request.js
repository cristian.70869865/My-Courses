/**
 * Created by BURGOS on 23/10/2018.
 */

const AuthorHttpResponse = require('./author.httpResponse');

class AuthorRequest {
  constructor () {}

  requestToPostAuthor (authorBody) {
    let authorModel = {
      name: null
    };

    if (!authorBody.name || authorBody === null)
      return authorModel;

    authorModel.name = authorBody.name;
    return authorModel;
  };

  requestToPutAuthor (authorBody) {
    let name = authorBody.name.trim();
    let authorModel = {
      name: ''
    };

    if (!name || authorBody === undefined)
      return AuthorHttpResponse.badRequest();

    authorModel.name = name;
    return authorModel;
  }
}

module.exports = AuthorRequest;