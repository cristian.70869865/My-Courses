/**
 * Created by BURGOS on 11/01/2018.
 */
const categoryModel = require('../models/category.model');

class CategoryController {
    constructor() {}

    getAllCategories(req, res) {
        categoryModel.find({}, (error, categories) => {
            if(error)
                return res.status(500).send({code: 500, data: {message:'Problema interno del servidor'}});
            if(categories.length === 0)
                return res.status(404).send({code: 404, data: {message:'Recurso no encontrado'}});
            res.status(200).send({code: 200, data: categories});
        })
    }

    getCategoryById(req, res) {
        let idCategory = req.params.id;
        categoryModel.findById(idCategory, (error, category) => {
            if(error)
                return res.status(404).send({code: 404, data: {message: 'Recurso no encontrado.'}});
            res.status(200).send({code: 200, data: category})
        })
    }

    postNewCategory(req, res) {
        let category  = new categoryModel();
        category.name = req.body.name;
        if(category.name === undefined)
            return res.status(400).send({code: 400, data: {message: 'Rellene todos los campos.'}});
        category.save( (error, categorySave) => {
            if(error)
                return res.status(500).send({code: 500, data: {message:'Problema interno del servidor'}});
            res.status(201).send({code: 201, data: categorySave});
        })
    }

    putCategory(req, res) {
        let idCategory   = req.params.id,
            bodyCategory = req.body;
        if(Object.entries(bodyCategory).length === 0)
            return res.status(400).send({code: 400, data: {message: 'Rellene todos los campos'}});
        categoryModel.findByIdAndUpdate(idCategory, bodyCategory, (error, updatedCategory) => {
            if(error)
                return res.status(404).send({code: 404, data: {message:'Recurso no encontrado'}});
            res.status(200).send({code: 200, data: updatedCategory});
        })
    }

    deleteCategory(req, res) {
        let idCategory = req.params.id;
        categoryModel.findById(idCategory, (error, deletingCategory) => {
            if(error !== null)
                return res.status(404).send({code: 404, data: {message: 'Recurso no encontrado.'}});
            deletingCategory.remove(error => {
                if(error)
                    return res.status(204).send({code: 204, data: {message:'Error al intentar borrar el registro.'}})
            });
            res.status(200).send({code: 200, data: {message: 'Se elimino correctamente'}});
        })
    }
}

module.exports = CategoryController;