/**
 * Created by BURGOS on 10/01/2018.
 */

const mongoose = require('mongoose'),
    schema = mongoose.Schema,
    platformSchema = schema({
        name: String
    });

module.exports = mongoose.model('platforms', platformSchema);