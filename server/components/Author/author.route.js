/**
 * Created by BURGOS on 14/01/2018.
 */

const AuthorController = require('./author.controller');
const AuthorMiddleware = require('./author.middleware');

class AuthorRoute {
  constructor(route) {
    this.route = route.Router();
    this.authorMiddleware = new AuthorMiddleware();
    this.authorController = new AuthorController();
  }

  getRoute() {
    this.defineRoutes();
    return this.route;
  }

  defineRoutes() {
    this.route.get('/author',
      this.authorController
        .getAllAuthors
        .bind(this.authorController)
    );
    this.route.get('/author/:id',
      this.authorController
        .getAuthorById
        .bind(this.authorController)
    );
    this.route.post('/author',
      this.authorController
        .postNewAuthor
        .bind(this.authorController)
    );
    this.route.put('/author/:id',
      this.authorMiddleware
        .validateFieldOfAuthor
        .bind(this.authorMiddleware),
      this.authorController
        .putAuthor
        .bind(this.authorController)
    );
    this.route.delete('/author/:id',
      this.authorController
        .deleteAuthor
        .bind(this.authorController)
    );
  }
}

module.exports = AuthorRoute;