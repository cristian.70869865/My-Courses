/**
 * Created by BURGOS on 11/01/2018.
 */

class CategoryRoute {
    constructor(route) {
        this.route = route.Router();
        this.categoryController = require('../controllers/category.controller')
    }

    getRoute() {
        this.defineRoutes();
        return this.route;
    }

    defineRoutes() {
        const category = new this.categoryController();
        this.route.get('/category', category.getAllCategories);
        this.route.get('/category/:id', category.getCategoryById);
        this.route.post('/category', category.postNewCategory);
        this.route.put('/category/:id', category.putCategory);
        this.route.delete('/category/:id', category.deleteCategory);
    }
}

module.exports = CategoryRoute;