/**
 * Created by BURGOS on 09/01/2018.
 */

const mongoose = require('mongoose'),
    schema = mongoose.Schema,
    placeSchema = schema({
        name: String
    });

module.exports = mongoose.model('places', placeSchema);